describe('Upload Document Testing', () => {
    beforeEach (() => {
        cy.viewport(1920, 1080);
        cy.clearLocalStorage('webapp-fe-dev.privylite.com');
        cy.clearCookies('webapp-fe-dev.privylite.com');
    });

    it ('Upload Document Sign n Request' , () => {
        
        const fileName = 'testFile.pdf';

        cy.visit('webapp-fe-dev.privylite.com');
        // Fill the username
        cy.get('[name="email"]').type('rimbafebriyan17@gmail.com')
        .should('have.value', 'rimbafebriyan17@gmail.com');

        // Fill the password
        cy.get('[name="password"]')
        .type('Akuntesrimba1')
        .should('have.value', 'Akuntesrimba1');

        //Login
        cy.get('[data-cy="submit"]').click()
        // .should('exist');

        cy.get('#v-upload > [target="_self"]').click();

        // start watching the POST requests
        cy.server({method:'POST'});
        // and in particular the one with 'upload_endpoint' in the URL
        cy.route('POST','/api/document/document/upload').as('upload');
    
        //Uploading File
        cy.fixture(fileName, 'binary')
            .then(Cypress.Blob.binaryStringToBlob)
            .then(fileContent => {
                cy.get('.form-upload > [type=file]').attachFile({
                    fileContent,
                    fileName,
                    mimeType: 'application/pdf',
                    encoding:'utf8',
                    lastModified: new Date().getTime()
                })
            });
        // wait for the 'upload_endpoint' request, and leave a 2 minutes delay before throwing an error
        // cy.wait('@upload');

        // stop watching requests
        // cy.server({ enable: false })

        // keep testing the app
        // e.g. cy.get('.link_file[aria-label="upload_1"]').contains('(pdf)')

        // //Kalo nama File ikut dengan yang diUpload
        // cy.get('.link > [target="_self"]').click();
        // cy.wait(2000)
        // //Delete Form
        // cy.get('.document-title > .break-words > .link').click();
        // cy.get('.btn-danger').click();

         //Kalo file nya pake nama
        cy.get('#title > [type=text]').type('Testing Upload Document');
        cy.get('.modal-detail > .btn-primary').click();
        cy.wait(8000);
        // Add New Recipient
        cy.get(':nth-child(1) > .recipient-add').click();
        cy.get('#recipient-email-search').type('rimbafebriyan@student.ub.ac.id');
        // cy.get('.dropdown-select__value').click();
        // cy.contains('Viewer').click();
        cy.get('.dropdown-select__value').click();
        cy.contains('Signer').click();
        cy.get('.recipient-form-flex').contains('Add').click();
        ////Add Viewer
        // cy.get('#recipient-email-search').type('ryugenzaki@gmail.com');
        // cy.get('.dropdown-select__value').click();
        // cy.contains('Viewer').click();
        // cy.get('.recipient-form-flex').contains('Add').click();
        
        // // Try Delete Signature
        // cy.get('#signature-draggable').contains('Signature').click();
        // cy.get('.recipient-list__item').contains('My Signature').click()
        // cy.get('.signature-action').contains('Delete Signature').click()
        
        // my Signature
        cy.get('#signature-draggable').contains('Signature').click();
        cy.get('.recipient-list > :nth-child(1)').contains('My Signature').click();
        cy.wait(8000);

        //Recipient Signature
        cy.get('#signature-draggable').contains('Signature').click();
        cy.get('.recipient-list > :nth-child(1)').click();

        //Drag n Drop Testing
        // cy.get(".pdf-pagination").then(($el) => {
        //   const x1 = $el[0].getBoundingClientRect().left;
        //   const x2 = $el[0].getBoundingClientRect().width;
        //   const xc = x1 + x2 / 2 ;
        //   const y1 = $el[0].getBoundingClientRect().top;
        //   const y2 = $el[0].getBoundingClientRect().height;
        //   const yc = y1 + y2 / 2 ;
        //   cy.get(".pdf-draggable")
        //     .trigger("mousedown")
        //     .trigger("mousemove", { clientX: xc, clientY: yc })
        //     .trigger("mouseup");
        // });

        cy.get('.upload-reason > #reasonMessage').type('Hayo jangan sampe kekirim ke orang loh ya !');
        cy.get('.btn-primary').contains('Review and Confirm to Sign').click();
        cy.wait(8000)

        cy.get('#emailMessage').type('<3')
        cy.get('.message-footer > .btn-primary').click()
        
     });

    it('Upload Document Self Sign' , () => {
       
       const fileName = 'testFile.pdf';

       cy.visit('webapp-fe-dev.privylite.com');
       // Fill the username
       cy.get('[name="email"]').type('rimbafebriyan17@gmail.com')
       .should('have.value', 'rimbafebriyan17@gmail.com');

       // Fill the password
       cy.get('[name="password"]')
       .type('Akuntesrimba1')
       .should('have.value', 'Akuntesrimba1');

       //Login
       cy.get('[data-cy="submit"]').click()
       // .should('exist');

       cy.get('#v-upload > [target="_self"]').click();

       // start watching the POST requests
       cy.server({method:'POST'});
       // and in particular the one with 'upload_endpoint' in the URL
       cy.route('POST','/api/document/document/upload').as('upload');
       // cy.route( 'GET', '/api/document/documents/gmwlaqth9pi0p2yrqxpf3d9j43wdjpf9214jfkstpp8k8tunutjw2jq6ucdbcpt56iraqgx3ikf5phv2/download?c=sign').as('upload');

       //Uploading File

       cy.fixture(fileName, 'binary')
           .then(Cypress.Blob.binaryStringToBlob)
           .then(fileContent => {
               cy.get('.form-upload > [type=file]').attachFile({
                   fileContent,
                   fileName,
                   mimeType: 'application/pdf',
                   encoding:'utf8',
                   lastModified: new Date().getTime()
               })
           })

        //Kalo file nya pake nama
       cy.get('#title > [type=text]').type('Testing Upload Document');
       cy.get('.modal-detail > .btn-primary').click();
       cy.wait(4000);

       // my Signature
       cy.get('#signature-draggable').contains('Signature').click();
       cy.contains('My Signature').click();
       cy.wait(8000);

       cy.get('.upload-reason > #reasonMessage').type('Hayo jangan sampe kekirim ke orang loh ya !');
       cy.get('.btn-primary').contains('Review and Confirm to Sign').click();
       cy.wait(8000)

       cy.get('#emailMessage').type('<3')
       cy.get('.message-footer > .btn-primary').click()
       
    });

    it ('Upload Document Req from Other' , () => {
        
        const fileName = 'testFile.pdf';

        cy.visit('webapp-fe-dev.privylite.com');
        // Fill the username
        cy.get('[name="email"]').type('rimbafebriyan17@gmail.com')
        .should('have.value', 'rimbafebriyan17@gmail.com');

        // Fill the password
        cy.get('[name="password"]')
        .type('Akuntesrimba1')
        .should('have.value', 'Akuntesrimba1');

        //Login
        cy.get('[data-cy="submit"]').click()
        // .should('exist');

        cy.get('#v-upload > [target="_self"]').click();

        // start watching the POST requests
        cy.server({method:'POST'});
        // and in particular the one with 'upload_endpoint' in the URL
        cy.route('POST','/api/document/document/upload').as('upload');
    
        //Uploading File
        cy.fixture(fileName, 'binary')
            .then(Cypress.Blob.binaryStringToBlob)
            .then(fileContent => {
                cy.get('.form-upload > [type=file]').attachFile({
                    fileContent,
                    fileName,
                    mimeType: 'application/pdf',
                    encoding:'utf8',
                    lastModified: new Date().getTime()
                })
            });
        // wait for the 'upload_endpoint' request, and leave a 2 minutes delay before throwing an error
        // cy.wait('@upload');

        // stop watching requests
        // cy.server({ enable: false })

        // keep testing the app
        // e.g. cy.get('.link_file[aria-label="upload_1"]').contains('(pdf)')

        // //Kalo nama File ikut dengan yang diUpload
        // cy.get('.link > [target="_self"]').click();
        // cy.wait(2000)
        // //Delete Form
        // cy.get('.document-title > .break-words > .link').click();
        // cy.get('.btn-danger').click();

         //Kalo file nya pake nama
        cy.get('#title > [type=text]').type('Testing Upload Document');
        cy.get('.modal-detail > .btn-primary').click();
        cy.wait(4000);
        // Add New Recipient
        cy.get(':nth-child(1) > .recipient-add').click();
        cy.get('#recipient-email-search').type('rimbafebriyan@student.ub.ac.id');
        // cy.get('.dropdown-select__value').click();
        // cy.contains('Viewer').click();
        cy.get('.dropdown-select__value').click();
        cy.contains('Signer').click();
        cy.get('.recipient-form-flex').contains('Add').click();

        //Recipient Signature
        cy.get('#signature-draggable').contains('Signature').click();
        cy.get('.recipient-list > :nth-child(2)').click();

        cy.get('.btn-primary').contains('Review and Confirm to Sign').click();
        cy.wait(4000)

        cy.get('#emailMessage').type('<3')
        cy.get('.message-footer > .btn-primary').click()
        
     });

});

   